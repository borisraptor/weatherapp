//
//  URLs.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.//

import Foundation

struct URLs {
    
    static func getUnitVal() -> String {
        if NSLocale.isMetric(){
            return "metric"
        }else{
            return "imperial"
        }
    }
    
    //MARK: Addresses
    static let BASE = "http://api.openweathermap.org/"
    static let DATA_URL_KEY = "data/"
    static let IMG_URL_KEY = "img/w/"
    static let API_VERSION = "2.5"
    static let CURRENT_WEATHER_URL_KEY = "weather"
    static let UNITS_KEY = "units"
    static let FORECAST_KEY = "forecast/"
    static let FORECAST_DAILY_KEY = "daily"

    //MARK: Keys
    static let WEATHER_API_KEY = "d7418e843db991b7d22ea177ebd45593"
    
    static let REQ_APP_ID_KEY = "APPID"
    static let REQ_CITY_KEY = "q"
    static let REQ_LAT_KEY = "lat"
    static let REQ_LON_KEY = "lon"
    static let REQ_ZIP_KEY = "zip"
    
    //MARK: api urls
    //api.openweathermap.org/data/2.5/weather?q=London&APPID=d7418e843db991b7d22ea177ebd45593
    static var currentWeatherUrl:String{
        return BASE + DATA_URL_KEY + API_VERSION + "/" + CURRENT_WEATHER_URL_KEY
    }
    
    //api.openweathermap.org/data/2.5/forecast/daily?q=London&units=metric&APPID=d7418e843db991b7d22ea177ebd45593
    static var forecastWeatherUrl:String{
        return BASE + DATA_URL_KEY + API_VERSION + "/" + FORECAST_KEY + FORECAST_DAILY_KEY
    }
    
    //MARK: Image URls
    static func getImgUrlForImg(imgFile: String) -> String {
        return BASE + IMG_URL_KEY + imgFile + ".png"
    }
    
    //MARK: param funcs    
    static func getParamDictWithCityName(name: String) -> [String : String] {
        return [ REQ_CITY_KEY : name, REQ_APP_ID_KEY : WEATHER_API_KEY, UNITS_KEY : getUnitVal() ]
    }
    static func getParamDictWithZip(zip: String) -> [String : String] {
        return [ REQ_ZIP_KEY : zip, REQ_APP_ID_KEY : WEATHER_API_KEY, UNITS_KEY : getUnitVal() ]
    }
    static func getParamDictWithlat(lat: String, andLon lon: String) -> [String : String] {
        return [ REQ_LAT_KEY : lat , REQ_LON_KEY : lon, REQ_APP_ID_KEY : WEATHER_API_KEY, UNITS_KEY : getUnitVal() ]
    }
    

}































