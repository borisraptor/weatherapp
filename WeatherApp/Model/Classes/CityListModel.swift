//
//  CityListModel.swift
//  WeatherApp
//
//  Created by boris barac on 5/2/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import UIKit
import GoogleMaps

class CityListModel: NSObject, NSCoding {
    
    //keys
    let DICT_KEY_NAME = "DICT_KEY_NAME"
    let DICT_KEY_FORMATED_ADR = "DICT_KEY_FORMATED_ADR"
    let DICT_KEY_LAT = "DICT_KEY_LAT"
    let DICT_KEY_LON = "DICT_KEY_LON"
    
    //attributes
    var name:String?
    var formattedAddress:String?
    //because they are used in the requests an strings
    var latitude:String?
    var longitude:String?
    
    override init() {}
    
    //NSCoding
    required init(coder aDecoder: NSCoder) {
        if let name = aDecoder.decodeObjectForKey(DICT_KEY_NAME) as? String {
            self.name = name
        }
        if let adr = aDecoder.decodeObjectForKey(DICT_KEY_FORMATED_ADR) as? String {
            self.formattedAddress = adr
        }
        if let lat = aDecoder.decodeObjectForKey(DICT_KEY_LAT) as? String {
            self.latitude = lat
        }
        if let lon = aDecoder.decodeObjectForKey(DICT_KEY_LON) as? String {
            self.longitude = lon
        }
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        if let name = self.name, let adr = self.formattedAddress, let lat = self.latitude, let lon = self.longitude {
            aCoder.encodeObject(name, forKey: DICT_KEY_NAME)
            aCoder.encodeObject(adr, forKey: DICT_KEY_FORMATED_ADR)
            aCoder.encodeObject(lat, forKey: DICT_KEY_LAT)
            aCoder.encodeObject(lon, forKey: DICT_KEY_LON)
        }
    }
    
    static func getCityFromPlace(place: GMSPlace) -> CityListModel {
        let city = CityListModel()
        city.name = place.name
        
        let arr = place.formattedAddress?.characters.split(",").map(String.init)
        
        //some times formated address contains name 2 times
        if arr?.count == 3 {
            city.formattedAddress = arr![0] + ", " + arr![2]
        }else{
            city.formattedAddress = place.formattedAddress
        }
        debugPrint(city.formattedAddress)
        city.latitude = String(place.coordinate.latitude)
        city.longitude = String(place.coordinate.longitude)
        
        return city
    }
    
}










