//
//  CurrentWeatherData.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.//

import Foundation
import SwiftyJSON
import RealmSwift

class BaseCity: Object {
    
    dynamic var coord:Coord?
    dynamic var id:Int = 0// not ? because it's a PK
    
}

class CurrentWeatherData: BaseCity {
    
    //MARK: Vars
    dynamic var weather:Weather?
    dynamic var main:Main?
    dynamic var wind:Wind?
    dynamic var clouds:Clouds?
    dynamic var rain:Rain?
    dynamic var snow:Snow?
    
    dynamic var country:String?
    let sunrise = RealmOptional <Double>()
    let sunset = RealmOptional <Double>()
    
    
    dynamic var cityName:String?
    
    //MARK: Realm entity setup
    override static func indexedProperties() -> [String] {
        return ["cityName"]
    }
    
    //MARK: Parse JSON
    static func parseJson(json: JSON) -> CurrentWeatherData {
        let data = CurrentWeatherData()
        
        // if nil returns 0.0
        data.id = json["id"].intValue
        data.cityName = json["name"].string
        
        data.sunrise.value = json["sys"]["sunrise"].double
        data.sunset.value = json["sys"]["sunset"].double
        data.country = json["sys"]["country"].string
        
        data.coord = Coord.parseJson(json["coord"])
        data.weather = Weather.parseJson(json["weather"])
        data.main = Main.parseJson(json["main"])
        data.wind = Wind.parseJson(json["wind"])
        data.clouds = Clouds.parseJson(json["clouds"])
        
        //can be empty, vol in the last 3hours
        data.rain = Rain.parseJson(json["rain"])
        data.snow = Snow.parseJson(json["snow"])
        
        return data
    }
    
}

class Coord: Object {
    
    let lon = RealmOptional <Double>()
    let lat = RealmOptional <Double>()
    
    static func parseJson(json: JSON) -> Coord {
        let data = Coord()
        data.lat.value = json["lat"].double
        data.lon.value = json["lon"].double
        
        return data
    }
}

class Weather: Object {
    
    let id = RealmOptional <Double>()
    dynamic var main:String?
    dynamic var weatherDescription:String?
    dynamic var icon:String?
    
    static func parseJson(json: JSON) -> Weather {
        let data = Weather()
        
        data.id.value = json[0]["id"].double
        data.main = json[0]["main"].string
        data.weatherDescription = json[0]["description"].string
        data.icon = json[0]["icon"].string
        
        return data
    }
    
}

class Main: Object {
    
    let temp = RealmOptional <Double>()
    let pressure = RealmOptional <Double>()
    let humidity = RealmOptional <Double>()
    let temp_min = RealmOptional <Double>()
    let temp_max = RealmOptional <Double>()
    
    static func parseJson(json: JSON) -> Main {
        let data = Main()
        
        data.temp.value = json["temp"].double
        data.pressure.value = json["pressure"].double
        data.humidity.value = json["humidity"].double
        data.temp_min.value = json["temp_min"].double
        data.temp_max.value = json["temp_max"].double
        
        return data
    }
    
}

class Wind: Object {
    
    let speed = RealmOptional <Double>()
    let deg = RealmOptional <Double>()
    
    static func parseJson(json: JSON) -> Wind {
        let data = Wind()
        
        data.speed.value = json["speed"].double
        data.deg.value = json["deg"].double
        
        return data
    }
}

class Clouds: Object {
    
    let all = RealmOptional <Double>()
    
    static func parseJson(json: JSON) -> Clouds {
        let data = Clouds()
        
        data.all.value = json["all"].double
        
        return data
    }
}


class Rain: Object {
    //    "rain" : {
    //    "3h" : 3
    //    }
    dynamic var vol:Int = 0
    
    static func parseJson(json: JSON) -> Rain {
        let data = Rain()
        
        if let val = json["3h"].int {data.vol = val }
        
        return data
    }
}

class Snow: Object {
    //    "snow" : {
    //    "3h" : 3
    //    }
    dynamic var vol:Int = 0
    
    static func parseJson(json: JSON) -> Snow {
        let data = Snow()
        
        if let val = json["3h"].int {
            data.vol = val
        }
        
        return data
    }
}





















