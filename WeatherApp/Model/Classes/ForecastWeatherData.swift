//
//  ForecastWeatherData.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.//

import Foundation
import SwiftyJSON
import RealmSwift

class ForecastWeatherData: Object {
    
    //num of days
    dynamic var cnt:Int = 0
    let list = List <ForecastDayData>()
    dynamic var cityForecastData:CityForecastData?
    
    //MARK: Realm entity setup
    override static func indexedProperties() -> [String] {
        return ["cnt"]
    }
    
    static func parseJson(json: JSON) -> ForecastWeatherData {
        let data = ForecastWeatherData()
        data.cnt = json["cnt"].intValue
        data.cityForecastData = CityForecastData.parseJson(json["city"])
        
        let arrayJson = json["list"]
        for (index, subJson) : (String, JSON) in arrayJson {
            debugPrint(index); debugPrint(subJson)
            data.list.append(ForecastDayData.parseJson(subJson))
        }
        
        return data
    }
}

//city info in this json
class CityForecastData: BaseCity {
    
    dynamic var name:String?
    dynamic var country:String?
    
    static func parseJson(json: JSON) -> CityForecastData {
        let data = CityForecastData()
        data.id = json["id"].intValue
        data.name = json["name"].string
        data.country = json["country"].string
        
        data.coord = Coord.parseJson(json["coord"])
        
        return data
    }
}

class ForecastDayData: Object {

    //date for the forecast
    let dt = RealmOptional <Double>()
    
    let pressure = RealmOptional <Double>()
    let humidity = RealmOptional <Double>()
    let speed = RealmOptional <Double>()
    let deg = RealmOptional <Double>()
    let clouds = RealmOptional <Double>()
    
    dynamic var temp:ForecastDayTemp?
    dynamic var weather:Weather?
    
    static func parseJson(json: JSON) -> ForecastDayData {
        let data = ForecastDayData()
        
        data.dt.value = json["dt"].double
        data.pressure.value = json["pressure"].double
        data.humidity.value = json["humidity"].double
        data.speed.value = json["speed"].double
        data.deg.value = json["deg"].double
        data.clouds.value = json["clouds"].double
        
        data.temp = ForecastDayTemp.parseJson(json["temp"])
        data.weather = Weather.parseJson(json["weather"])
        
        return data
    }
    
}

class ForecastDayTemp: Object {
    
    let day = RealmOptional <Double>()
    let min = RealmOptional <Double>()
    let max = RealmOptional <Double>()
    let night = RealmOptional <Double>()
    let eve = RealmOptional <Double>()
    let morn = RealmOptional <Double>()
    
    static func parseJson(json: JSON) -> ForecastDayTemp {
        let data = ForecastDayTemp()
        
        data.day.value = json["day"].double
        data.min.value = json["min"].double
        data.max.value = json["max"].double
        data.night.value = json["night"].double
        data.eve.value = json["eve"].double
        data.morn.value = json["morn"].double
        
        return data
    }

}
