//
//  Requests.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift

final class CurrentWeatherReq: VCRequestProtocol {
    
    static var isWorking = false
    
    //MARK: VCRequestProtocol
    weak var vc:BaseViewController!
    
    var lastTimeFired:NSDate? = nil
    var error:ReqError? = nil
    var data:JSON? = nil
    
    let realm = try! Realm()
    
    //don't use this
    required init(){
    }
    
    func postNotification(ref: Bool) {
        NSNotificationCenter.defaultCenter().postNotificationName(NotificationKeys.currentReqDone.rawValue, object: nil)
        if !ref {   self.vc.hud?.hide(true) }
    }
    
    /**
     gets the data, changes it in memory and in cashe
     if the is a network error checks for the cahed data, and returns that one if it exists
     
     - parameter ref: activated from refresh
     */
    func makeReqfromRefresh(ref: Bool) {
        
        if CurrentWeatherReq.isWorking {
            return
        }
        
        // if city not set check current location
        if LocationManager.sharedInstance.overloadedLocSet == false {
            guard let _ = LocationManager.sharedInstance.currentLocation else{
                debugPrint("cant get location")
                
                if let todayVC = self.vc as? TodayViewController{
                    todayVC.currentData = nil
                    todayVC.tableView.reloadData()
                }
                
                return
            }
        }
        
        self.lastTimeFired = NSDate()
        
        if !ref {
            if let todayVC = self.vc as? TodayViewController{
                if todayVC.tableView.numberOfRowsInSection(0) == 0{
                    self.vc.hud = self.vc.showHud()
                }
            }
        }else{
            (self.vc as! TodayViewController).refCon.beginRefreshing()
        }
        
        CurrentWeatherReq.isWorking = true
        
        let (latStr, lonStr) = self.getLocationParameters()// in the VCRequestProtocol extension
        
        //make the req
        Alamofire.request(.GET, URLs.currentWeatherUrl, parameters: URLs.getParamDictWithlat(latStr, andLon: lonStr))
            .response { [unowned self](req, res, data, err) in
                CurrentWeatherReq.isWorking = false
//                debugPrint(req)
//              debugPrint(res); debugPrint(data); debugPrint(err)
                if let e = err {
                    print(e.localizedDescription)
                    self.error = ReqError.createFromErr(e)
                }
            }
            .responseJSON { [unowned self](res) in
                if res.data?.length > 0 {
                    let json = JSON(data:res.data!)// data never nil
                    
                    //check if api ret an error
                    if json["cod"].intValue == 404{
                        self.error = ReqError.noWeatherData
                        self.postNotification(ref)
                    }else{
                        //no api err,parse and cashe
                        self.data = json
                        
                        if let vc = self.vc as? TodayViewController {
                            
                            NSOperation.executeBlock({
                                //changes data in memory
                                vc.currentData = CurrentWeatherData.parseJson(json)
                                }, inQueue: NSOperationQueue(),
                                completion: { (finished) in
                                    assert(vc.currentData != nil, "parsing should never fails")
                                    
                                    let arr = self.realm.objects(CurrentWeatherData).filter(self.getPredicateForLat(Double(latStr)!, andLon:Double(lonStr)!, forForecast: false))
                                    
                                    do{
                                        try self.realm.write({
                                            self.realm.delete(arr)
                                        })
                                        try self.realm.write({
                                            self.realm.add(vc.currentData!)
                                        })
                                    }catch let err as NSError{
                                        debugPrint(err.localizedDescription)
                                    }
                                    //memory object changed, cashe changed
                                    self.postNotification(ref)
                            })
                        }
                    }
                    
                }else {
                    // no data, use the cashed data if it's there
                    if let vc = self.vc as? TodayViewController {
                        let arr = self.realm.objects(CurrentWeatherData).filter(self.getPredicateForLat(Double(latStr)!, andLon:Double(lonStr)!, forForecast: false))
                        vc.currentData = arr.first
                    }
                    self.postNotification(ref)
                }
            }
    }
    
}











