//
//  NetworkProtocols.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.


import Foundation
import UIKit
import CFNetwork
import SwiftyJSON

//MARK: helper enums
//NSError implemets ErrorType
enum ReqError:ErrorType {
    
    case noNetwork
    case reqErr
    case urlErr
    case authRequred
    case authErr
    case deff
    case noWeatherData
    
    func titleForErr() -> String {
        switch self {
        case .noNetwork:
            return localizeStringForKey("No network connection.")
        case .authRequred:
            return localizeStringForKey("Please log in.")
        case .authErr:
            return localizeStringForKey("Wrong username or password.")
        case .noWeatherData:
            return localizeStringForKey("This location is not supported.")
        default:
            return ""
        }
    }
    
    static func createFromErr(err: NSError) -> ReqError {
        if err.code == NSURLErrorNotConnectedToInternet{
            return ReqError.noNetwork
        }else if err.domain == NSURLErrorDomain {
            return ReqError.urlErr
        }else if err.code == NSURLErrorUserAuthenticationRequired {
            return ReqError.authRequred
        }else if err.code == Int(CFNetworkErrors.CFErrorHTTPBadCredentials.rawValue) {
            return ReqError.authErr
        }else{
            return ReqError.deff
        }
    }
    
}

//MARK: VCRequestProtocol
//init as a atribute in a VC, dont share between multiple objects
protocol VCRequestProtocol {
    
    weak var vc:BaseViewController! { get set }
    
    var lastTimeFired:NSDate? { get set }
    var error:ReqError? { get set }
    var data:JSON? { get set }
    
    init()
    init(viewController: BaseViewController)
    
    func makeReqfromRefresh(ref: Bool)
    
}

//MARK: extension VCRequestProtocol
extension VCRequestProtocol{
    
    init(viewController: BaseViewController){
        self.init()
        vc = viewController
    }
    
    //this will make Earth weather data parameters, if it's not set to some location
    func getLocationParameters() -> (String, String) {
        //prepare req for current location
        var latStr = ""
        var lonStr = ""
        if let curLoc = LocationManager.sharedInstance.currentLocation{
            latStr = String(curLoc.coordinate.latitude)
            lonStr = String(curLoc.coordinate.longitude)
        }
        
        //checks for user set city
        if let lat = LocationManager.sharedInstance.overloadedCity?.latitude, let lon = LocationManager.sharedInstance.overloadedCity?.longitude {
            latStr = lat
            lonStr = lon
        }
        
        return (latStr, lonStr)
    }
    
    //predicates
    func getPredicateForLat(lat: Double, andLon lon: Double, forForecast forecast: Bool) -> NSPredicate {
        //forecast data
        if forecast{
            return NSPredicate(format: "cityForecastData.coord.lat > %f AND cityForecastData.coord.lat < %f AND cityForecastData.coord.lon > %f AND cityForecastData.coord.lon < %f" , lat - epsilon, lat + epsilon, lon - epsilon, lon + epsilon)
        
        }// currentData
        else{
            return NSPredicate(format: "coord.lat > %f AND coord.lat < %f AND coord.lon > %f AND coord.lon < %f" , lat - epsilon, lat + epsilon, lon - epsilon, lon + epsilon)
        }
    }
    
}































