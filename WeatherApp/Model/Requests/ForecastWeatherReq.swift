//
//  File.swift
//  WeatherApp
//
//  Created by boris barac on 5/15/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift

final class ForecastWeatherReq: VCRequestProtocol{
    
    static var isWorking = false
    
    //MARK: VCRequestProtocol
    weak var vc:BaseViewController!
    
    var lastTimeFired:NSDate? = nil
    var error:ReqError? = nil
    var data:JSON? = nil
    
    let realm = try! Realm()
    
    required init(){
    }
    
    func postNotification(ref: Bool) {
        NSNotificationCenter.defaultCenter().postNotificationName(NotificationKeys.forecastReqDone.rawValue, object: nil)
        if !ref {   self.vc.hud?.hide(true) }
    }
    
    /**
     gets the data, changes it in memory and in cashe
     if the is a network error checks for the cahed data, and returns that one if it exists
     
     - parameter ref: activated from refresh
     */
    func makeReqfromRefresh(ref: Bool) {
        if ForecastWeatherReq.isWorking{
            return
        }
        
        // if city not set check current location
        if LocationManager.sharedInstance.overloadedLocSet == false {
            guard let _ = LocationManager.sharedInstance.currentLocation else{
                debugPrint("cant get location")
                
                if let forcastVC = self.vc as? ForecastViewController{
                    forcastVC.forecastData.list.removeAll()
                    forcastVC.forecastData.cityForecastData = nil
                    forcastVC.forecastData.cnt = 0
                    forcastVC.tableView.reloadData()
                }
                
                return
            }
        }
        
        self.lastTimeFired = NSDate()
        
        if !ref {
            if let forcastVC = self.vc as? ForecastViewController{
                if forcastVC.tableView.numberOfRowsInSection(0) == 0{
                    self.vc.hud = self.vc.showHud()
                }
            }
        }else{
            (self.vc as! ForecastViewController).refCon.beginRefreshing()
        }
        
        ForecastWeatherReq.isWorking = true
        
        let (latStr, lonStr) = self.getLocationParameters()
        
        //make req :)
        Alamofire.request(.GET, URLs.forecastWeatherUrl, parameters: URLs.getParamDictWithlat(latStr, andLon: lonStr))
            .response { [unowned self](req, res, data, err) in
                ForecastWeatherReq.isWorking = false
                //                debugPrint(req); debugPrint(res); debugPrint(data); debugPrint(err);
                if let e = err {
                    print(e.localizedDescription)
                    self.error = ReqError.createFromErr(e)
                }
            }
            .responseJSON { [unowned self](res) in
                if res.data?.length > 0 {
                    let json = JSON(data:res.data!)//data is never nil
                    
                    //check if api ret an error
                    if json["cod"].intValue == 404{
                        self.error = ReqError.noWeatherData
                        self.postNotification(ref)
                    }else{
                        //no api err,parse and cashe
                        self.data = json
                        
                        if let vc = self.vc as? ForecastViewController {
                            
                            NSOperation.executeBlock({ 
                                //changes data in memory
                                vc.forecastData = ForecastWeatherData.parseJson(json)
                                }, inQueue: NSOperationQueue(),
                                completion: { (finished) in
                                    let arr = self.realm.objects(ForecastWeatherData).filter(self.getPredicateForLat(Double(latStr)!, andLon:Double(lonStr)!, forForecast: true))
                                    
                                    do{
                                        try self.realm.write({
                                            self.realm.delete(arr)
                                        })
                                        try self.realm.write({
                                            self.realm.add(vc.forecastData)
                                        })
                                    }catch let err as NSError{
                                        debugPrint(err.localizedDescription)
                                    }
                                    //memory object changed, cashe changed
                                    self.postNotification(ref)
                            })
                        }
                    }
                    
                }else {
                    // no data, use the cashed data if it's there
                    if let vc = self.vc as? ForecastViewController {
                        let arr = self.realm.objects(ForecastWeatherData).filter(self.getPredicateForLat(Double(latStr)!, andLon:Double(lonStr)!, forForecast: true))
                        if let first = arr.first{
                            vc.forecastData = first}
                    }
                    self.postNotification(ref)
                }
            }
    }
    
}

