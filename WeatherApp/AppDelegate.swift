//
//  AppDelegate.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import UIKit
import GoogleMaps
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window:UIWindow?
    
    //MARK: Custom
    func setUpRealm() {
        var config = Realm.Configuration()
        config.fileURL = NSURL(string: NSTemporaryDirectory())!.URLByAppendingPathComponent("LocalData.realm")
        
        // Set this as the configuration used for the default Realm
        Realm.Configuration.defaultConfiguration = config
    }
    
    func setUpIpad() {
        if isIpad(){
            let sb = UIStoryboard(name:"Main", bundle: nil)
            let mVC = sb.instantiateViewControllerWithIdentifier(ViewControllerIds.Today.rawValue) as! TodayViewController
            let dVC = sb.instantiateViewControllerWithIdentifier(ViewControllerIds.Forecast.rawValue) as! ForecastViewController
            
            let nav1 = NavigationController(rootViewController:mVC)
            let nav2 = NavigationController(rootViewController:dVC)
            
            let sVC = SplitViewController()
            sVC.viewControllers = [nav1, nav2]
            sVC.delegate = mVC
            
            window = UIWindow(frame: UIScreen.mainScreen().bounds)
            window?.rootViewController = sVC
            window?.makeKeyAndVisible()
        }
    }
    
    //MARK: System
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject : AnyObject]?) -> Bool {
        
        //google autocomplete api
        GMSServices.provideAPIKey(kGoogleAPIKey)
        
        //saves the vendor id because it's changing on the simulator all the time
        //FIXME: Remove NSUserDefaults identifierForVendor at release version
        let id = UIDevice.currentDevice().identifierForVendor!.UUIDString
        if NSUserDefaults.standardUserDefaults().objectForKey("unID") == nil{
            NSUserDefaults.standardUserDefaults().setObject(id, forKey: "unID")
        }

        setUpRealm()
        
        setUpIpad()
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

