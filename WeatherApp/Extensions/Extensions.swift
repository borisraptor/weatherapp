//
//  Extensions.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import Foundation
import DZNEmptyDataSet

extension UITableViewCell{
    
    func cellFlyInAnimationWithDuration(duration: Double, andDumping dumping: CGFloat, withInitialSpringVelocity isv: CGFloat) {
        self.frame = CGRectMake(UIScreen.mainScreen().bounds.width, self.frame.origin.y, self.frame.size.width, self.frame.size.height)
        
        UIView.animateWithDuration(duration, delay: 0, usingSpringWithDamping: dumping, initialSpringVelocity: isv, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            self.frame = CGRectMake(0, self.frame.origin.y, self.frame.size.width, self.frame.size.height)
            }, completion: { finished in
                
        })
    }
    
}

extension UITableView{
    
    func setupTableView(obj: protocol<UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate >) {
        self.dataSource = obj
        self.delegate = obj
        self.separatorStyle = .None
        
        self.emptyDataSetSource = obj
        self.emptyDataSetDelegate = obj
    }
    
}

extension NSOperation{
    
    static func executeBlock(block: () -> Void, inQueue queue: NSOperationQueue, completion: (finished: Bool) -> Void) -> NSOperation {
        let blockOperation = NSBlockOperation(block:block)
        let completionOperation = NSBlockOperation { 
            completion(finished: blockOperation.finished)
        }
        
        completionOperation.addDependency(blockOperation)
        NSOperationQueue.currentQueue()!.addOperation(completionOperation)
        queue.addOperation(blockOperation)
        
        return blockOperation
    }
    
}

extension NSLocale{
    
    static func isMetric() -> Bool {
        var isMetric = true
        let loc = NSLocale.currentLocale()
        
        if let met = loc.objectForKey(NSLocaleUsesMetricSystem)?.boolValue{
            isMetric = met
        }
        
        return isMetric
    }
    
}

extension String{
    
    func getNameForCountry() -> String? {
        let country = NSLocale(localeIdentifier:"en").displayNameForKey(NSLocaleCountryCode, value: self)
        //debugPrint(country!)
        return country
    }
    
    static func getCompassDir(deg: Int) -> String {
        if deg >= 315 || deg <= 45{
            return "E"
        }
        
        if deg >= 45 && deg <= 135{
            return "N"
        }
        
        if deg >= 135 && deg <= 225{
            return "W"
        }
        
        if deg >= 25 && deg <= 315{
            return "S"
        }else{
            return ""
        }
    }
    
}

extension Double{
    
    func toInt() -> Int {
        return Int(self)
    }

}


extension SWColor{
    
    static func getGreen() -> UIColor {
        return UIColor(red:0.16, green:0.63, blue:0.21, alpha:1.00)
    }

}







