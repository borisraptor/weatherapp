//
//  ForecastCell.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import UIKit
import Haneke

class ForecastCell: UITableViewCell {
    
    //MARK: outlets
    @IBOutlet weak var img:UIImageView!
    @IBOutlet weak var day:UILabel!
    @IBOutlet weak var weather:UILabel!
    @IBOutlet weak var temp:UILabel!
    
    //MARK: Vars
    var dayForecast:ForecastDayData?{
        didSet{
            //day
            if let dt = dayForecast?.dt.value{
                let date = NSDate(timeIntervalSince1970:dt)
                debugPrint(DateHelpre.getWeekDay(date))
                self.day.text = DateHelpre.getWeekDay(date)
            }else{
                day.text = localizeStringForKey("nn")
            }
            //weather
            debugPrint(dayForecast?.weather?.description)
            self.weather.text = dayForecast?.weather?.weatherDescription
            //temp
            if let day = dayForecast?.temp?.day.value{
                self.temp.text = String(day.toInt()) + "˚"
            }else{
                self.temp.text = localizeStringForKey("nn")
            }
            
            //image set
            debugPrint(dayForecast?.weather?.icon)
            if let iconName = dayForecast?.weather?.icon{
                self.img.hnk_setImageFromURL(NSURL(string: URLs.getImgUrlForImg(iconName)))
            }
        } 
    }

    //MARK: Override funcs
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .None
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
