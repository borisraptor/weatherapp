//
//  TodayCell.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import UIKit
import Haneke

protocol ForecastButtonClickDelegate {
    func forecastClick(cell: TodayCell)
}

class TodayCell: UITableViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var weatherImg:UIImageView!
    @IBOutlet weak var weatherDesc:UILabel!
    @IBOutlet weak var weatherTemp:UILabel!
    
    @IBOutlet weak var RainVal:UILabel!
    @IBOutlet weak var preasure:UILabel!
    @IBOutlet weak var wind:UILabel!
    @IBOutlet weak var humidity:UILabel!
    @IBOutlet weak var sunrise:UILabel!
    @IBOutlet weak var sunset:UILabel!
    @IBOutlet weak var curLocationImage:UIImageView!
    @IBOutlet weak var forecastButton:UIButton!
    
    //MARK: Vars
    var forecastDelegate:ForecastButtonClickDelegate?
    
    var data:CurrentWeatherData?{
        didSet{
            //img
                if let iconName = data?.weather?.icon{
                    self.weatherImg.hnk_setImageFromURL(NSURL(string: URLs.getImgUrlForImg(iconName)))
                }
            //temp
            var unit = ""
            if NSLocale.isMetric(){ unit = " ˚C" } else { unit = " ˚F" }
            if let temp = data?.main?.temp.value{
                self.weatherTemp.text = String(temp.toInt()) + unit
            }
            //weather description
            if let des = data?.weather?.weatherDescription{
                self.weatherDesc.text = des
            }else { self.weatherDesc.text = "" }
            
            //rain Vol
            if let rainVol = data?.rain?.vol{
                RainVal.text = String(rainVol) + localizeStringForKey(" mm/3h")
            }else { RainVal.text = localizeStringForKey("nn") }
            //presure
            if let pressure = self.data?.main?.pressure.value{
                self.preasure.text = String(pressure.toInt()) + localizeStringForKey(" hPa")
            } else { self.preasure.text = localizeStringForKey("nn") }
            //wind
            if let wSpeed = self.data?.wind?.speed.value{
                let unit = NSLocale.isMetric() ? localizeStringForKey(" m/sec") : localizeStringForKey(" mil/h")
                self.wind.text = String(wSpeed.toInt()) + unit
            } else { self.wind.text = localizeStringForKey("nn") }
            //humidity
            if let humidity = self.data?.main?.humidity.value{
                self.humidity.text = String(humidity) + " %"
            } else { self.humidity.text = localizeStringForKey("nn") }
            
            //sunrise
            if let unixTime = self.data?.sunrise.value{
                self.sunrise.text = DateHelpre.getTimeFromUnixTime(unixTime)
            } else { self.sunrise.text = localizeStringForKey("nn") }
            //sunset
            if let unixTime = self.data?.sunset.value{
                self.sunset.text = DateHelpre.getTimeFromUnixTime(unixTime)
            } else { self.sunset.text = localizeStringForKey("nn") }
            
            if LocationManager.sharedInstance.overloadedLocSet{
                self.curLocationImage.hidden = true
            }else{
                self.curLocationImage.hidden = false
            }

        }
    }
    
    //MARK: Actions
    func showHideForecastButton() {
        guard isIpad() else{
            return
        }
        
        if UIApplication.sharedApplication().keyWindow?.traitCollection.horizontalSizeClass == .Compact{
            self.forecastButton.hidden = false
        }else{
            self.forecastButton.hidden = true
        }
    }
    
    @IBAction func forecastClick(sender: AnyObject) {
        self.forecastDelegate?.forecastClick(self)
    }
    
    //MARK: System
    override func layoutSubviews() {
        super.layoutSubviews()
        showHideForecastButton()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .None
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    
}












