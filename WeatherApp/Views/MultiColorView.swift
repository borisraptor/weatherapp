//
//  MultiColorView.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class MultiColorView: UIView {
    
    //vars
    @IBInspectable var firstColor:UIColor = UIColor(red:0.92, green:0.40, blue:0.76, alpha:1.00)
    @IBInspectable var secondColor:UIColor = UIColor(red:0.99, green:0.53, blue:0.31, alpha:1.00)
    @IBInspectable var thirdColor:UIColor = UIColor(red:0.16, green:0.63, blue:0.21, alpha:1.00)
    @IBInspectable var fourthColor:UIColor = UIColor(red:0.21, green:0.58, blue:0.99, alpha:1.00)
    @IBInspectable var fifthColor:UIColor = UIColor(red:1.00, green:0.80, blue:0.27, alpha:1.00)
    @IBInspectable var sixthColor:UIColor = UIColor(red:0.84, green:0.22, blue:0.13, alpha:1.00)
    
    var colorArr:Array <UIColor> {
        return [fifthColor, secondColor, thirdColor, fourthColor, fifthColor, sixthColor]
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        let w = rect.width / 6
        for i in 0...5{
            let ind = CGFloat(i)
            let r = CGRect(x:ind * w, y: 0, width: w, height: rect.height)
            colorArr[i].set()
            UIRectFill(r)
        }
    }
    
}
