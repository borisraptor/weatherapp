//
//  CityListViewController.swift
//  WeatherApp
//
//  Created by boris barac on 5/1/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import UIKit
import GoogleMaps
import DZNEmptyDataSet

final class CityListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Outlets
    @IBOutlet weak var tableView:UITableView!
    
    //MARK: Vars
    var autocompleteController:GMSAutocompleteViewController = {
        let acc = GMSAutocompleteViewController()
        acc.modalPresentationStyle = .CurrentContext
        
        return acc
    }()
    
    //datasource
    // CityListModel is used because GMSPlace is not serializable, and 90% of it is READ only
    var cities:[CityListModel] = {
        //get data for data source
        if let data = NSUserDefaults.standardUserDefaults().objectForKey(UserDefautsKeys.cityArray.rawValue) as? NSData {
            if let cityArray = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [CityListModel] {
                return cityArray
            }
        }
        
        return [CityListModel]()
    }()
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.setupTableView(self)
        self.tableView.allowsMultipleSelection = false
        self.autocompleteController.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        //save dataSource to user defs
        let cityList = NSKeyedArchiver.archivedDataWithRootObject(cities)
        NSUserDefaults.standardUserDefaults().setObject(cityList, forKey: UserDefautsKeys.cityArray.rawValue)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        debugPrint("city count is: \( self.cities.count )")
    }
    
    //MARK: Actions
    @IBAction func exitClick(sender: AnyObject) {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func addClick(sender: AnyObject) {
        self.presentViewController(autocompleteController, animated: true, completion: nil)
        // Work around an iOS bug where the main event loop sometimes doesn't wake up to process the
        // presentation of the view controller
        dispatch_async(dispatch_get_main_queue()) {}
    }
    
    //MARK: UITableViewDelegate && UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView.dequeueReusableCellWithIdentifier(CellIDs.CityList.rawValue) == nil  {
            tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: CellIDs.CityList.rawValue)
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier(CellIDs.CityList.rawValue)
        cell?.textLabel?.textAlignment = .Center
        cell?.selectionStyle = .Gray
        
        if indexPath.row == 0{
            cell?.textLabel?.text = localizeStringForKey("Current location")
        }else{
            cell?.textLabel?.text = self.cities[indexPath.row - 1].formattedAddress
        }
        
        //never nil
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0 {
            
            guard LocationManager.sharedInstance.locationServicesOK else{
                let aVC = UIAlertController(title: "Location error", message: "Please enable location services in the settings.", preferredStyle: .Alert)
                
                let cancelAction = UIAlertAction(title: localizeStringForKey("Cancel"), style: .Cancel, handler: nil)
                let okAction = UIAlertAction(title: localizeStringForKey("OK"), style: .Default, handler: { (action) in
                    openSettings()
                })
                
                aVC.addAction(cancelAction)
                aVC.addAction(okAction)
                
                self.presentViewController(aVC, animated: true, completion: nil)
                
                self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
                
                return
            }
            
            LocationManager.sharedInstance.overloadedCity = nil
        }else{
            LocationManager.sharedInstance.overloadedCity = cities[indexPath.row - 1]
            LocationManager.sharedInstance.currentLocation = nil
        }
        
        if isIpad(){//because of popover
            NSNotificationCenter.defaultCenter().postNotificationName(NotificationKeys.citySelected.rawValue, object: nil)
        }
        
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MRAK: TableView edit functions
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return indexPath.row == 0 ? false : true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete{
            self.cities.removeAtIndex(indexPath.row - 1)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        }
    }
    
}

//MARK: GMSAutocompleteViewControllerDelegate
extension CityListViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(viewController: GMSAutocompleteViewController, didAutocompleteWithPlace place: GMSPlace) {
        debugPrint("Place name: ", place.name)
        debugPrint("Place address: ", place.formattedAddress)
        debugPrint("Place cordinate: ", place.coordinate)
        
        let cityModel = CityListModel.getCityFromPlace(place)
        
        // check if it's in the data source
        var contained = true
       
        if cities.count == 0{
            contained = false
        }
        
        for city in cities {
            if city.formattedAddress == cityModel.formattedAddress{
                break
            }else{
                contained = false
            }
        }
        
        if contained == false{
            self.cities.append(cityModel)
            self.tableView.reloadData()
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func viewController(viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: NSError) {
        // TODO: handle the error.
        debugPrint("Error: ", error.description)
    }
    
    // User canceled the operation.
    func wasCancelled(viewController: GMSAutocompleteViewController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
}

//MARK: DZNEmptyDataSetSource
extension CityListViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    
    func emptyDataSetShouldAllowTouch(scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let attributes:[String : AnyObject] = [NSFontAttributeName: UIFont.boldSystemFontOfSize(18.0), NSForegroundColorAttributeName: UIColor.darkGrayColor()]
        let title = localizeStringForKey("No cities, please add some.")
        
        return NSAttributedString(string: title, attributes: attributes)
    }
    
}
























