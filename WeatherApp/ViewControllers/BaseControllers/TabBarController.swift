//
//  TabBarController.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    //MARK: Override
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.tintColor = SWColor.getGreen()
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if isIpad(){
            return .AllButUpsideDown
        }else{
            return .Portrait
        }
    }
    
}






