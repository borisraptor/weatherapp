//
//  NavigationViewController.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.//

import UIKit

class NavigationController: UINavigationController {

    //MARK: Override
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.tintColor = SWColor.getGreen()
        navigationBar.translucent = false
        
        let shadow = NSShadow()
        shadow.shadowOffset = CGSizeMake(0, 0.5)
        shadow.shadowColor = SWColor.blackColor()
        
        navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName : SWColor.getGreen(), NSShadowAttributeName : shadow ]
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if isIpad(){
            return .AllButUpsideDown
        }else{
            return .Portrait
        }
    }
    
}
