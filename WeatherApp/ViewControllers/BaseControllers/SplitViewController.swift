//
//  SplitViewController.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import UIKit

class SplitViewController: UISplitViewController {
    
    //MARK: Vars
    //1/3 or 2/3 of the screen width
    var screenRationRegular:Bool{
        if let window = UIApplication.sharedApplication().keyWindow{
            return window.bounds.width > UIScreen.mainScreen().bounds.width * 0.4
        }
        
        return false
    }
    
    //size class overload
    override var traitCollection:UITraitCollection{
        if isIpadPro() &&  screenRationRegular {
            return UITraitCollection(horizontalSizeClass: .Regular)
        }
        
        return super.traitCollection
    }

    //MARK: Sustem funcs
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if self.traitCollection.horizontalSizeClass == .Regular {
            let kMasterViewWidth:CGFloat = UIApplication.sharedApplication().keyWindow!.bounds.width / 2
            let masterViewController:UIViewController = self.viewControllers[0]
            let detailViewController:UIViewController = self.viewControllers[1]
            if detailViewController.view.frame.origin.x > 0.0 {

                var masterViewFrame:CGRect = masterViewController.view.frame
                let deltaX:CGFloat = masterViewFrame.size.width - kMasterViewWidth
                masterViewFrame.size.width -= deltaX
                masterViewController.view.frame = masterViewFrame

                var detailViewFrame:CGRect = detailViewController.view.frame
                detailViewFrame.origin.x -= deltaX
                detailViewFrame.size.width += deltaX
                detailViewController.view.frame = detailViewFrame
                masterViewController.view!.setNeedsLayout()
                detailViewController.view!.setNeedsLayout()
            }
        }
    }

}
