//
//  BaseViewController.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.//

import UIKit
import MBProgressHUD

class BaseViewController: UIViewController {
    
    //MARK: Vars
    var netReq:protocol <VCRequestProtocol>? = nil
    var hud:MBProgressHUD? = nil
    var refCon = UIRefreshControl()
    
    //MARK: System
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.delegate = self
        
        NSNotificationCenter.defaultCenter().addObserverForName(NotificationKeys.citySelected.rawValue, object: nil, queue: NSOperationQueue.mainQueue()) { (_) in
            self.netReq?.makeReqfromRefresh(false)
        }
    }
    
    //MARK: DZdataSetFunctions
    func DZTitle() -> NSAttributedString {
        var title = "There has been an error. :("
        if LocationManager.sharedInstance.currentLocation == nil {
            title = localizeStringForKey("Trying to find your location.")
        }
        
        if LocationManager.sharedInstance.generalLocationServices == false {
            title = localizeStringForKey("This Aplication needs location services to work.")
        }else
            if LocationManager.sharedInstance.whenInUsePer == false {
                title = localizeStringForKey("This Aplication needs a when in use permission to work.")
            }else
                if let req = self.netReq{
                    if let err = req.error{
                        if err == ReqError.noNetwork || err == ReqError.noWeatherData {
                            title = err.titleForErr()   }
                    }
        }
        
        let attributes:[String : AnyObject] = [NSFontAttributeName: UIFont.boldSystemFontOfSize(18.0), NSForegroundColorAttributeName: UIColor.darkGrayColor()]
        
        return NSAttributedString(string: title, attributes: attributes)
    }
    
    func DZButtonTitle(forState state: UIControlState) -> NSAttributedString {
        var title = ""
        
        if LocationManager.sharedInstance.generalLocationServices == false || LocationManager.sharedInstance.whenInUsePer == false {
            title = localizeStringForKey("Location Services")
        }else{
            if let err = self.netReq?.error {
                if err == ReqError.noNetwork {
                    title = localizeStringForKey("Network Settings")
                }
            }
        }
        
        let font = UIFont.systemFontOfSize(15.0)
        let textColor = SWColor(hexString: (state == .Normal) ? "#007ee5" : "#48a1ea")
        let attributes:[String : AnyObject] = [NSFontAttributeName: font, NSForegroundColorAttributeName: textColor!]
        
        return NSAttributedString(string: title, attributes: attributes)
    }
    
    func DZButtonAction() {
        if LocationManager.sharedInstance.generalLocationServices == false || LocationManager.sharedInstance.whenInUsePer == false {
            openSettings()
        }else{
            if let err = self.netReq?.error{
                if err == ReqError.noNetwork{
                    openSettings()
                }
            }
        }
    }
    
    //MARK: Custom funcs
    func showHud() -> MBProgressHUD? {
        //debugPrint("hud Shown")
        var hud:MBProgressHUD?
        if let nav = self.tabBarController{
            hud = MBProgressHUD.showHUDAddedTo(nav.view, animated: true)
        }else{
            hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        }
        
        hud?.labelText = localizeStringForKey("Loading")
        hud?.labelColor = UIColor.whiteColor()
        hud?.dimBackground = true
        hud?.activityIndicatorColor = UIColor.whiteColor()
       
        return hud
    }
    
    func shareText(sharingText: String?, sharingImage: UIImage?, sharingURL: NSURL?, barButton: UIBarButtonItem) {
        var sharingItems = [AnyObject]()
        
        if let text = sharingText {
            sharingItems.append(text)
        }
        
        if let image = sharingImage {
            sharingItems.append(image)
        }
        
        if let url = sharingURL {
            sharingItems.append(url)
        }
        
        let activityViewController = UIActivityViewController(activityItems:sharingItems, applicationActivities: nil)
        
        if isIpad(){
            activityViewController.modalPresentationStyle = .Popover
            let popOver = activityViewController.popoverPresentationController
            popOver?.barButtonItem = barButton
        }
        
        self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
}

//MARK: UITabBarControllerDelegate
extension BaseViewController: UITabBarControllerDelegate{
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        self.netReq?.makeReqfromRefresh(false)
    }
    
}









