//
//  TodayViewController.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import Haneke

final class TodayViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource  {
    
    //MARK: Outlets
    @IBOutlet weak var tableView:UITableView!
    
    //MARK: Vars
    var currentData:CurrentWeatherData?
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.setupTableView(self)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.currentReqDone), name: NotificationKeys.currentReqDone.rawValue, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.locationChanged), name: NotificationKeys.locationChange.rawValue, object: nil)
        NSNotificationCenter.defaultCenter().addObserverForName(UIApplicationDidBecomeActiveNotification, object: nil, queue: NSOperationQueue.mainQueue(), usingBlock: { notification in
            //called everu time app appears, except on the first launch on the iPhone (apple bug)
            self.tableView.reloadData()//err handling
            self.netReq?.makeReqfromRefresh(false)//if some err was fixed makes the req again
        })
        
        self.netReq = CurrentWeatherReq(viewController: self)
        self.setUpRefresh()
        
        //ipad version only
        self.splitViewController?.preferredDisplayMode = UISplitViewControllerDisplayMode.AllVisible
    }
    
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.netReq?.makeReqfromRefresh(false)
        self.tableView.reloadData()
    }
    
    //MARK: Notification Actions
    func currentReqDone() {
        //ui changes
        //data is changed and parsed in the req
        if self.refCon.refreshing{
            self.refCon.endRefreshing()
        }
        
        self.tableView.reloadData()
        self.changeNabBarTitle()
    }
    
    func locationChanged() {
        debugPrint("location changed")
        self.netReq?.makeReqfromRefresh(false)
    }
    
    //MARK: Refersh
    func setUpRefresh() {
        refCon.backgroundColor = SWColor.getGreen()
        refCon.tintColor = UIColor.whiteColor()
        refCon.attributedTitle = NSAttributedString(string: localizeStringForKey("Refreshing weather."), attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
        refCon.addTarget(self, action: #selector(self.refreshPull), forControlEvents: .ValueChanged)
        tableView.addSubview(refCon)
    }
    
    func refreshPull() {
        debugPrint("refresh pulled")
        self.netReq?.makeReqfromRefresh(true)
    }
    
    //MARK: Actions
    @IBAction func shareAction(sender: AnyObject) {
        self.shareText(localizeStringForKey("Check out this app"), sharingImage: nil, sharingURL: NSURL(string: "www.apple.com"), barButton: sender as! UIBarButtonItem)
    }
    
    func changeNabBarTitle() {
        var title = ""
        
        if let city = LocationManager.sharedInstance.overloadedCity{
            if let adr = city.formattedAddress{
                title = adr
            }
        }else {
            if let cName = currentData?.cityName{//this retuns a city district address
                title = cName
                if let country = currentData?.country?.getNameForCountry() {
                    title = cName + ", " + country
                }
            }
        }
        
        self.navigationItem.title = title
    }
    
    //MARK: Table delegate && data
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = self.currentData?.cityName{
            return 1
        }else{
            return 0
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableView.frame.height
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCellWithIdentifier(CellIDs.Today.rawValue) as? TodayCell{
            cell.data = self.currentData
            cell.forecastDelegate = self
            
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.cellFlyInAnimationWithDuration(0.75, andDumping: 0.4, withInitialSpringVelocity: 0.6)
    }
    
}

//MARK: ForecastButtonClickDelegate
extension TodayViewController: ForecastButtonClickDelegate{
    
    func forecastClick(cell: TodayCell) {
        if let _ = self.splitViewController{
            let fVC = self.storyboard?.instantiateViewControllerWithIdentifier(ViewControllerIds.Forecast.rawValue) as! ForecastViewController
            self.showViewController(fVC, sender: self)
        }
    }
    
}

//MARK: DZNEmptyDataSet
extension TodayViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    
    func emptyDataSetShouldAllowTouch(scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        return self.DZTitle()  //baseVC
    }
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
        return self.DZButtonTitle(forState: state)   //baseVC
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        self.DZButtonAction()     //baseVC
    }
    
}

//MARK: UISplitViewControllerDelegate
extension TodayViewController: UISplitViewControllerDelegate{
    
    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController: UIViewController, ontoPrimaryViewController primaryViewController: UIViewController) -> Bool {
        return true
    }
    
}


















