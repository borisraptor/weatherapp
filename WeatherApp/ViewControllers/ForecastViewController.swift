//
//  ViewController.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import MBProgressHUD

final class ForecastViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK:Outlets
    @IBOutlet weak var tableView:UITableView!
    
    //MARK: Vars
    var forecastData = ForecastWeatherData()
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.setupTableView(self)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.forecastReqDone), name: NotificationKeys.forecastReqDone.rawValue, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.locationChanged), name: NotificationKeys.locationChange.rawValue, object: nil)
        NSNotificationCenter.defaultCenter().addObserverForName(UIApplicationDidBecomeActiveNotification, object: nil, queue: NSOperationQueue.mainQueue(), usingBlock: { notification in
            //called everu time app appears, except on the first launch on the iPhone (apple bug)
            self.tableView.reloadData()//err handling
            self.netReq?.makeReqfromRefresh(false)//if some err was fixed makes the req again
        })
        
        if isIpad(){
            self.navigationItem.rightBarButtonItem = nil
            self.navigationItem.leftBarButtonItem = nil
        }
        
        self.netReq = ForecastWeatherReq(viewController: self)
        
        //sets up refresh controll
        self.setUpRefresh()
    }
    
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.netReq?.makeReqfromRefresh(false)
        self.tableView.reloadData()
    }
    
    //MARK: Notification Actions
    func forecastReqDone() {
        //ui changes
        if self.refCon.refreshing{
            self.refCon.endRefreshing()
        }
        
        self.tableView.reloadData()
        self.changeNavBarTitle()
    }
    
    func locationChanged() {
        debugPrint("location changed")
        self.netReq?.makeReqfromRefresh(false)
    }
    
    //MARK: Refersh
    func setUpRefresh() {
        refCon.backgroundColor = SWColor.getGreen()
        refCon.tintColor = UIColor.whiteColor()
        refCon.attributedTitle = NSAttributedString(string: localizeStringForKey("Refreshing weather."), attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
        refCon.addTarget(self, action: #selector(self.refreshPull), forControlEvents: .ValueChanged)
        tableView.addSubview(refCon)
    }
    
    func refreshPull() {
        debugPrint("refresh pulled")
        self.netReq?.makeReqfromRefresh(true)
    }
    
    //MARK: Actions
    @IBAction func shareAction(sender: AnyObject) {
        self.shareText(localizeStringForKey("Check out this app"), sharingImage: nil, sharingURL: NSURL(string: "www.apple.com"), barButton: sender as! UIBarButtonItem)
    }
    
    func changeNavBarTitle() {
        if isIpad(){
            self.navigationItem.title = localizeStringForKey("Forecast")
        }else{
            var title = ""
            
            if let city = LocationManager.sharedInstance.overloadedCity{
                if let adr = city.formattedAddress{
                    title = adr
                }
            }else {
                if let cName = forecastData.cityForecastData?.name{//this retuns a city district address
                    title = cName
                    if let country = forecastData.cityForecastData?.country?.getNameForCountry() {
                        title = cName + ", " + country
                    }
                }
            }
            
            self.navigationItem.title = title
        }
    }
    
    //MARK: TabelView methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.forecastData.list.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 82
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CellIDs.Forecast.rawValue) as? ForecastCell
        if let cell = cell{
            cell.dayForecast = self.forecastData.list[indexPath.row]
            
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.cellFlyInAnimationWithDuration(0.75, andDumping: 0.4, withInitialSpringVelocity: 0.6)
    }
    
}

//MARK: DZNEmptyDataSet
extension ForecastViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    
    func emptyDataSetShouldAllowTouch(scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        return self.DZTitle()  //baseVC
    }
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
        return self.DZButtonTitle(forState: state)   //baseVC
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        self.DZButtonAction()     //baseVC
    }
    
}







