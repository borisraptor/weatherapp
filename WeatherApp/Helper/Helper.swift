//
//  Helper.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import Foundation

class DateHelpre {
    
    static let formatter = NSDateFormatter()
    
    static func getWeekDay(date: NSDate) -> String {
        formatter.dateFormat = "EEEE"
        //debugPrint(formatter.stringFromDate(date).capitalizedString)
        return formatter.stringFromDate(date).capitalizedString
    }
    
    static func getTimeFromUnixTime(secs: Double) -> String {
        formatter.dateFormat = "HH:mm"
        let date = NSDate(timeIntervalSince1970:secs)
        
        return formatter.stringFromDate(date)
    }
    
}