//
//  LocationMenager.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import UIKit
import CoreLocation

class LocationManager:NSObject, CLLocationManagerDelegate {

    static let sharedInstance = LocationManager()
    
    //MARK: Vars
    let manager = CLLocationManager()
    var timer:NSTimer?
    
    //location vars
    var currentLocation:CLLocation?
    var currentLocSet:Bool{
        if let _ = currentLocation{
            return true
        }
        
        return false
    }
    
    var overloadedCity:CityListModel?
    var overloadedLocSet:Bool{
        if let _ = overloadedCity{
            return true
        }
        
        return false
    }
    
    //premissions
    var generalLocationServices:Bool {
        return CLLocationManager.locationServicesEnabled()
    }
    
    var whenInUsePer:Bool {
        return  (CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse)
    }
    
    var locationServicesOK:Bool{
        return ( generalLocationServices && whenInUsePer )
    }
    
    func updateLocation(){
        manager.requestLocation()
    }
    
    //MARK: System
    override init() {
        super.init()
        
        manager.delegate = self
        manager.distanceFilter = kCLDistanceFilterNone
        manager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        manager.requestWhenInUseAuthorization()
        manager.requestLocation()
        
        //update location every 20 minutes
        timer = NSTimer(timeInterval: 20 * 60, target: self, selector: Selector(updateLocation()), userInfo: nil, repeats: true)
    }
    
    deinit{
        timer?.invalidate()
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        if locationServicesOK{
            manager.requestLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        self.currentLocation = nil
        manager.requestLocation()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.currentLocation = locations.first
        NSNotificationCenter.defaultCenter().postNotificationName(NotificationKeys.locationChange.rawValue, object: nil)
    }
    
    
}
























    

