//
//  Global.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import Foundation
import UIKit

//This file is used for global functions and variables that are project wide

let epsilon = 0.02//floating point comparison diffrence, api, and GMPlace, same city

let kGoogleAPIKey = "AIzaSyDIAkvywBBTustuNsPjFAGySikhc9D9nvs"

func openSettings() {
    UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
}

func isIpadPro() -> Bool {
    return isIpad() && UIScreen.mainScreen().bounds.height == 1366
}

func isIpad() -> Bool {
    if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
        return true
    }else{
        return false
    }
}

func isPortrait() -> Bool {
    return UIDevice.currentDevice().orientation.isPortrait
}


func localizeStringForKey(key:String!) -> String {
    return NSLocalizedString(key, comment: "")
}
