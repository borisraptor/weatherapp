//
//  IDs.swift
//  WeatherApp
//
//  Created by boris barac on 4/30/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import Foundation

enum UserDefautsKeys:String {
    
    case cityArray = "CITY_ARR_KEY"

}

enum ViewControllerIds:String {
    
    case Forecast = "ForecastViewController"
    case Today = "TodayViewController"

}

enum CellIDs: String {
    
    case Forecast = "ForecastCell"
    case Today = "TodayCell"
    case CityList = "cityListCell"

}

enum NotificationKeys:String {
    
    case currentReqDone = "currentReqDone"
    case forecastReqDone = "forecastReqDone"
    case locationChange = "locationChange"
    case citySelected = "citySelected"
    
}