**Weather App example project**
========================

#### Notes:
> - This is a continuous development version some bugs are possible, some features may not be complete 100.

#### Functionality:
> - Universal APP
> - iPad split screen support
> - Realm cashing
> - City List with name autocomplete
> - Pull to refresh
> - Network error handling
> - Permissions error handling
> - Location error handling
> - Use of NSLocale unit system

#### Coding Highlights:
> - Unit testing ( some tests need to be run separately )
> - Custom network protocol
> - Protocol extensions
> - Custom delegates
> - Protocol attributes and function parameters
> - @IBDesignable custom views
> - NSCoding compliant classes
> - And all the usual SWIFT stuff :)

#### Problems:
> - LIBRARY BUG: DZEmptyDataSet title sometimes moves from the center
> - This project is done without a designer
> - Use of default api images for weather

#### To Do:
> - Bug fixes
