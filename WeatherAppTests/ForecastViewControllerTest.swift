//
//  ForecastViewControllerTest.swift
//  WeatherApp
//
//  Created by boris barac on 6/17/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import XCTest
import CoreLocation
import DZNEmptyDataSet

@testable import WeatherApp




/*  READ ME
 If the test fails please run it separately, the application is using a lot of singletons, and all of the tests are actually run in the same app container at the same time by XCode.
 */


class ForecastViewControllerTest: XCTestCase {
    
    var forecastVC:ForecastViewController!
    var locationManager:LocationManager!
    
    let cLat = 50.06
    let cLon = 14.42
    
    override func setUp() {
        super.setUp()
        
        //continueAfterFailure = true
        let storyBoard = UIStoryboard(name:"Main", bundle: nil)
        forecastVC = storyBoard.instantiateViewControllerWithIdentifier(ViewControllerIds.Forecast.rawValue) as! ForecastViewController
        UIApplication.sharedApplication().keyWindow!.rootViewController = forecastVC
        
        LocationManager.sharedInstance.currentLocation = CLLocation(latitude: cLat, longitude: cLon)
        LocationManager.sharedInstance.overloadedCity = CityListModel()
        
        //preload view
        let _ = forecastVC!.view
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
//        LocationManager.sharedInstance.currentLocation = nil
//        LocationManager.sharedInstance.overloadedCity = nil
        super.tearDown()
    }
    
    func testDelegates() {
        XCTest.conformsToProtocol(UITableViewDataSource)
        XCTest.conformsToProtocol(UITableViewDelegate)
        XCTest.conformsToProtocol(DZNEmptyDataSetSource)
        XCTest.conformsToProtocol(DZNEmptyDataSetDelegate)
    }
    
    func testReqType() {
        XCTAssertTrue(self.forecastVC.netReq is ForecastWeatherReq, "Wrong netReq type")
    }
    
    func testParallelRequestsFlag() {
        let req = ForecastWeatherReq(viewController:BaseViewController())
        
        req.makeReqfromRefresh(false)
        XCTAssertTrue(ForecastWeatherReq.isWorking, "multiple same requests")
    }
    
    func testRequest() {
        //when i designed the app i din't want to be possible to have 2 same requests at the same time
        //disable parallel networking safeguards for the same req for the unit test
        ForecastWeatherReq.isWorking = false
        
        let expectation = expectationWithDescription("waiting for forecast weather req to be done, testRequest test")
        
        LocationManager.sharedInstance.currentLocation = CLLocation(latitude: cLat, longitude: cLon)
        LocationManager.sharedInstance.overloadedCity = CityListModel()
        forecastVC?.netReq?.makeReqfromRefresh(false)
        
        expectationForNotification(NotificationKeys.forecastReqDone.rawValue, object: nil) {
            notification in
            expectation.fulfill()
            
            return true
        }
        
        waitForExpectationsWithTimeout(30) { (err) in
            if let e = err{
                XCTFail("\(e.debugDescription)")
            }
            
            XCTAssertNil(self.forecastVC!.netReq!.error, "request failed")
        }
    }
    
    func testParsing() {
        //when i designed the app i din't want to be possible to have 2 same requests at the same time
        //disable parallel networking safeguards for the same req for the unit test
        ForecastWeatherReq.isWorking = false
        
        let expectationParsing = expectationWithDescription("waiting for the req to finish and parse the data, testParsing test")
        
        LocationManager.sharedInstance.currentLocation = CLLocation(latitude: cLat, longitude: cLon)
        LocationManager.sharedInstance.overloadedCity = CityListModel()
        forecastVC?.netReq?.makeReqfromRefresh(false)
        
        expectationForNotification(NotificationKeys.forecastReqDone.rawValue, object: nil) {
            notification in
            expectationParsing.fulfill()
            
            return true
        }
        
        waitForExpectationsWithTimeout(30) { (err) in
            if let e = err{
                XCTFail("\(e.debugDescription)")
            }
            
            let req = self.forecastVC.netReq as! ForecastWeatherReq
            XCTAssertNotNil(req, "request is nil")
            
            let data = req.data
            XCTAssertNotNil(data, "No json data")
            
            if let data = req.data{
                self.forecastVC.forecastData = ForecastWeatherData.parseJson(data)
                XCTAssertNotNil(self.forecastVC.forecastData, "data is nil")
            }else{
                XCTFail()
            }
            
        }
    }
    
    func testRequestFailed() {
        forecastVC.netReq?.error = ReqError.noNetwork
        //delete cashe
        forecastVC.forecastData.cityForecastData = nil
        forecastVC.tableView.reloadData()
        XCTAssertTrue(forecastVC.tableView.numberOfRowsInSection(0) == 0)
    }
    
    func testRealmCashing() {
        var exp = XCTestExpectation()
        
        //when the req is done data is cashed
        LocationManager.sharedInstance.currentLocation = CLLocation(latitude: self.cLat, longitude: self.cLon)
        LocationManager.sharedInstance.overloadedCity = CityListModel()
        
        ForecastWeatherReq.isWorking = false
        forecastVC?.netReq?.makeReqfromRefresh(false)
        
        exp = self.expectationWithDescription("waiting for forecast weather req to be done, testRealmCashing test")
        expectationForNotification(NotificationKeys.forecastReqDone.rawValue, object: nil) {
            notification in
            exp.fulfill()
            return true
        }
        
        waitForExpectationsWithTimeout(30) { (err) in
            if let e = err{
                XCTFail("\(e.debugDescription)")
            }
            
            let req = self.forecastVC.netReq as! ForecastWeatherReq
            let arr = req.realm.objects(ForecastWeatherData).filter(req.getPredicateForLat(self.cLat, andLon:self.cLon, forForecast: true))
            
            XCTAssertNotNil(arr)
        }
    }
    
    
    
    
    
    
    
    
    
    
    
}
















