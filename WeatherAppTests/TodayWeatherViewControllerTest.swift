//
//  TodayWeatherViewControllerTest.swift
//  WeatherApp
//
//  Created by boris barac on 6/16/16.
//  Copyright © 2016 boris_barac. All rights reserved.
//

import XCTest
import CoreLocation
import DZNEmptyDataSet

@testable import WeatherApp



/*  READ ME
If the test fails please run it separately, the application si using a lot of singletons, and all of the tests are actually run in the same app container at the same time by Xcode.
 */

class TodayWeatherViewControllerTest: XCTestCase {
    
    var currentVC:TodayViewController!
    
    let cLat = 50.06
    let cLon = 14.42
    
    override func setUp() {
        super.setUp()
        
        let storyBoard = UIStoryboard(name:"Main", bundle: nil)
        currentVC = storyBoard.instantiateViewControllerWithIdentifier(ViewControllerIds.Today.rawValue) as! TodayViewController
        UIApplication.sharedApplication().keyWindow!.rootViewController = currentVC
        
        LocationManager.sharedInstance.currentLocation = CLLocation(latitude: cLat, longitude: cLon)
        LocationManager.sharedInstance.overloadedCity = CityListModel()
        
        //preload view
        let _ = currentVC!.view
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        
//                LocationManager.sharedInstance.currentLocation = nil
//                LocationManager.sharedInstance.overloadedCity = nil
        super.tearDown()
    }
    
    func testDelegates() {
        XCTest.conformsToProtocol(UITableViewDataSource)
        XCTest.conformsToProtocol(UITableViewDelegate)
        XCTest.conformsToProtocol(DZNEmptyDataSetSource)
        XCTest.conformsToProtocol(DZNEmptyDataSetDelegate)
    }
    
    func testReqType() {
        XCTAssertTrue(self.currentVC?.netReq is CurrentWeatherReq, "Wrong netReq type")
    }
    
    func testParallelRequests() {
        let req = CurrentWeatherReq(viewController:BaseViewController())
        
        req.makeReqfromRefresh(false)
        XCTAssertTrue(CurrentWeatherReq.isWorking, "multiple same requests")
    }
    
    func testRequest() {
        //when i designed the app i din't want to be possible to have 2 same requests at the same time
        //disable parallel networking safeguards for the same req for the unit test
        CurrentWeatherReq.isWorking = false
        
        let expectation = expectationWithDescription("waiting for current weather req to be done")
        
        
        //to avoid location getting lost in the tests
        LocationManager.sharedInstance.currentLocation = CLLocation(latitude: cLat, longitude: cLon)
        LocationManager.sharedInstance.overloadedCity = CityListModel()//overloaded location is a computed property
        currentVC?.netReq?.makeReqfromRefresh(false)
        
        expectationForNotification(NotificationKeys.currentReqDone.rawValue, object: nil) {
            notification in
            expectation.fulfill()
            return true
        }
        
        waitForExpectationsWithTimeout(30) { (err) in
            if let e = err{
                XCTFail("\(e.debugDescription)")
            }
            
            XCTAssertNil(self.currentVC!.netReq!.error, "request failed")
        }
    }
    
    func testParsingAndDataPassing() {
        //when i designed the app i din't want to be possible to have 2 same requests at the same time
        //disable parallel networking safeguards for the same req for the unit test
        CurrentWeatherReq.isWorking = false
        
        let expectation = expectationWithDescription("waiting for the req to finish and parse the data")
        
        LocationManager.sharedInstance.currentLocation = CLLocation(latitude: cLat, longitude: cLon)
        LocationManager.sharedInstance.overloadedCity = CityListModel()
        currentVC?.netReq?.makeReqfromRefresh(false)
        
        expectationForNotification(NotificationKeys.currentReqDone.rawValue, object: nil) {
            notification in
            expectation.fulfill()
            return true
        }
        
        waitForExpectationsWithTimeout(30) { (err) in
            if let e = err{
                XCTFail("\(e.debugDescription)")
            }
            
            let req = self.currentVC.netReq as! CurrentWeatherReq
            XCTAssertNotNil(req, "request is nil")
            
            let data = req.data
            XCTAssertNotNil(data, "No json data")
            
            if let data = req.data{
                self.currentVC.currentData = CurrentWeatherData.parseJson(data)
                XCTAssertNotNil(self.currentVC.currentData, "data is nil")
            }else{
                XCTFail()
            }
            
        }
    }
    
    func testRequestFailed() {
        currentVC.netReq?.error = ReqError.noNetwork
        //delete cashe
        currentVC.currentData = nil
        currentVC.tableView.reloadData()
        XCTAssertTrue(currentVC.tableView.numberOfRowsInSection(0) == 0)
    }
    
    func testRealmCashing() {
        let expectation = expectationWithDescription("waiting for current weather req to be done, testRealmCashing test case")
        
        //when the req is done data is cashed
        LocationManager.sharedInstance.currentLocation = CLLocation(latitude: cLat, longitude: cLon)
        LocationManager.sharedInstance.overloadedCity = CityListModel()
        currentVC?.netReq?.makeReqfromRefresh(false)
        
        expectationForNotification(NotificationKeys.currentReqDone.rawValue, object: nil) {
            notification in
            expectation.fulfill()
            return true
        }
        
        waitForExpectationsWithTimeout(30) { (err) in
            if let e = err{
                XCTFail("\(e.debugDescription)")
            }
            
            let req = self.currentVC.netReq as! CurrentWeatherReq
            let arr = req.realm.objects(CurrentWeatherData).filter(req.getPredicateForLat(self.cLat, andLon:self.cLon, forForecast: false))
            
            XCTAssertNotNil(arr)
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
